---
title: Présentation
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Présentation
imgUrl: https://i.imgur.com/iGvihVf.jpg
description: En savoir plus sur moi!
layout: '../../layouts/BlogPost.astro'
type: 'blog'
---

### Conscience
En tant qu'ingénieur logiciel, mes valeurs humaines sont au cœur de mon approche professionnelle. Je suis particulièrement préoccupé par l'utilisation de l'intelligence artificielle et son impact. Je suis conscient que certaines technologies peuvent être utilisées de manière **abusive**, comme la surveillance de masse qui est utilisée pour censurer les opposants politiques dans certains gouvernements. Ces pratiques me préoccupent grandement et me poussent à réfléchir à la manière dont la technologie peut impacter notre **vie privée** et notre **liberté d'expression**.

### Humains

En plus de ma passion pour la technologie, j'ai une **profonde envie d'aider les autres**. J'aime partager mes connaissances avec les autres et aider ceux qui ont besoin d'assistance technique. Je suis également **curieux** de découvrir de nouvelles cultures et de nouvelles personnes et j'aime me faire de nouveaux amis. Cette curiosité et cette soif de découverte me poussent à sortir de ma zone de confort et à explorer de nouveaux domaines. J'essaie toujours de voir le monde sous de nouvelles perspectives et de comprendre les problèmes complexes d'**un point de vue plus large**. Je suis convaincu que cette curiosité et cet esprit d'ouverture sont essentiels pour réussir en tant qu'ingénieur logiciel, car ils me permettent de comprendre les besoins et les motivations des utilisateurs.

### Projets

En tant qu'ingénieur logiciel, je souhaite **impacter un maximum** de gens avec mes créations. Je suis passionné par la découverte et l'expérimentation de nouvelles technologies, car cela me permet d'explorer de nouvelles façons de répondre aux besoins des utilisateurs. Je suis convaincu que la technologie peut avoir un **impact positif sur la société** si elle est utilisée de manière **responsable**. Je suis toujours à la recherche de nouveaux projets personnels qui me permettent de tester les limites de mes compétences en programmation, que ce soit la création de jeux vidéo, l'exploration de l'intelligence artificielle ou la construction de robots. Ces projets me permettent de **me dépasser** et d'**affiner mes compétences** techniques, tout en satisfaisant ma **curiosité** et ma soif de découverte.

### Intérêts

En ce qui concerne mes centres d'intérêt, j'ai une passion pour l'animation japonaise et les jeux vidéo, car je trouve ces médias fascinants en termes de **narration, de conception artistique et d'innovation technologique**. Je suis particulièrement intéressé par la façon dont les animés et les jeux vidéo sont devenus des formes d'art populaire, intégrant des éléments de **musique**, d'**animation**, de graphisme et de **scénario** pour créer des expériences immersives et **émotionnellement riches**. J'apprécie explorer les univers créatifs et imaginaires présentés dans divers médias ainsi que les différentes techniques utilisées pour les réaliser. Les romans policiers sont également une de mes passions, car j'aime résoudre des **énigmes complexes**. En dehors de ma passion pour la technologie, je suis **curieux** de découvrir les différentes cuisines du monde, d'explorer de nouvelles saveurs et de nouveaux ingrédients et d'expérimenter avec la création de plats originaux. Cette passion pour la cuisine m'a également appris à être **patient et méthodique**, deux qualités qui sont également importantes en tant qu'ingénieur logiciel. Mes centres d'intérêt reflètent ma curiosité et mon désir d'explorer différents domaines de la vie, que ce soit dans le monde de l'art et de la culture ou dans celui de la gastronomie.
