---
title: Création d'un robot Discord
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Rust
 - Relation utilisateurs
 - UX
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1095232821838221332/bot_dislol.png
description: Un robot utilitaire et fun!
layout: '../../layouts/BlogPost.astro'
type: 'real'
---

# Réalisation d'un robot Discord en Rust

## Présentation du projet

Dans le cadre de mon projet personnel, j'ai décidé de créer un robot Discord en utilisant le langage de programmation Rust. Discord est une plateforme de communication en ligne très populaire utilisée principalement par les gamers, mais également par d'autres communautés pour discuter, partager des contenus et interagir en ligne. En créant un robot Discord, j'ai souhaité développer mes compétences en relation utilisateurs, UX (expérience utilisateur) et en Rust, un langage de programmation système performant et sûr.

## Objectif et contexte

L'objectif de ce projet était de créer un robot Discord qui pourrait effectuer diverses actions favorisant l'interaction entre les membres d'un serveur. Le robot devait être convivial et facile à utiliser, offrant une expérience utilisateur agréable aux membres du serveur Discord. Le contexte du projet était de créer un robot Discord en Rust, un langage de programmation relativement récent mais en pleine croissance, afin d'explorer ses fonctionnalités et sa communauté.

## Étapes de réalisation

La réalisation du robot Discord en Rust s'est déroulée en plusieurs étapes :

1. **Planification et conception** : J'ai commencé par planifier les fonctionnalités du robot en identifiant les actions automatisées nécessaires et en évaluant les exigences techniques. J'ai également conçu l'architecture du robot, en déterminant les différents modules et les interactions entre eux.
2. **Développement du robot** : J'ai utilisé le langage de programmation Rust pour développer le robot Discord. J'ai utilisé des bibliothèques Rust spécifiques pour interagir avec l'API Discord et implémenter les fonctionnalités du robot. J'ai veillé à respecter les bonnes pratiques de programmation Rust, telles que la gestion des erreurs, la sécurité et les performances.
3. **Tests et débogage** : J'ai effectué des tests rigoureux du robot pour m'assurer de son bon fonctionnement et de sa fiabilité. J'ai corrigé les éventuels bugs et amélioré les fonctionnalités pour offrir une expérience utilisateur optimale.
4. **Déploiement du robot** : Une fois que le robot a été développé et testé, j'ai procédé à son déploiement sur un serveur Discord réel. J'ai configuré les autorisations du robot, les rôles et les canaux nécessaires pour son bon fonctionnement.

## Acteurs

En tant que créateur du projet, j'ai travaillé seul sur la réalisation de ce robot Discord en Rust. J'ai été responsable de la planification, de la conception, du développement, des tests et du déploiement du robot.

## Résultats

Le robot Discord que j'ai créé en Rust fonctionne de manière fiable avec très peu de temps d'arrêt. Il est capable d'effectuer les actions automatisées pour lesquelles il a été conçu, offrant une expérience utilisateur conviviale aux membres du serveur Discord. Les interactions avec les utilisateurs sont fluides et les fonctionnalités du robot sont bien intégrées dans le serveur Discord.

## Avenir du projet

Dans un futur immédiat, je prévois de continuer à améliorer et à optimiser le robot Discord dans le but d'ajouter de nouvelles fonctionnalités en fonction des besoins de la communauté du serveur Discord. Je prévois également de mettre en place un processus de maintenance régulière pour assurer la stabilité et la performance du robot à long terme.

Aujourd'hui, le robot Discord que j'ai créé en Rust continue à fonctionner efficacement, offrant une expérience utilisateur satisfaisante aux membres du serveur Discord. En travaillant à distance, j'ai dû m'assurer de maintenir une communication efficace avec les membres du serveur et de résoudre rapidement tout problème ou bug éventuel. De plus, en utilisant Rust pour le développement du robot, j'ai pu profiter des avantages du langage en termes de sécurité, de performance et de robustesse, ce qui a contribué à minimiser les erreurs et à assurer un bon fonctionnement du robot.

## Mon regard critique

En tant que créateur du projet, j'ai pu apporter plusieurs contributions significatives à la réalisation du robot Discord en Rust. Tout d'abord, mes compétences en relation utilisateurs et en UX m'ont permis de concevoir une interaction conviviale et facile à utiliser pour les membres du serveur Discord, en veillant à ce que les fonctionnalités du robot soient accessibles et intuitives.

Les enseignements que j'ai retirés de ce projet sont nombreux. Tout d'abord, j'ai acquis une meilleure compréhension des fonctionnalités et de la communauté de Rust, un langage de programmation en constante évolution et très prometteur. J'ai également amélioré mes compétences en relation utilisateurs et en UX, en apprenant à concevoir des interfaces conviviales et à optimiser l'expérience utilisateur dans un contexte de développement de robot Discord. Enfin, j'ai également développé ma capacité à travailler de manière autonome, à gérer efficacement un projet de bout en bout, à résoudre les problèmes et à prendre des décisions importantes pour assurer le succès du projet.

### Compétences

 - [Rust](https://nagispace.gitlab.io/skill/rust/)
 - [Relation utilisateurs](https://nagispace.gitlab.io/skill/relation-ux/)
