---
title: Création d'un patch de traduction pour un jeux-vidéo
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Ingénierie inverse
 - Système
 - Java
 - Autonomie
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1095235491844079626/RanceX.png
description: Patcher les textes d'un jeu avec une version traduite automatiquement
layout: '../../layouts/BlogPost.astro'
type: 'real'
---

# Réalisation d'un patch de traduction pour un jeu utilisant une machine virtuelle propriétaire

## Présentation du projet

Le projet dont nous allons parler concerne la création d'un patch de traduction pour un jeu utilisant une machine virtuelle propriétaire. Le but de ce projet était de traduire les textes du jeu dans une autre langue afin de rendre le jeu jouable à mes amis.

## Objectif et contexte

L'objectif principal de ce projet était de permettre aux joueurs qui parlent une langue différente de celle du jeu original (Japonais) de profiter pleinement de l'expérience de jeu en comprenant les dialogues. Le jeu utilisait une machine virtuelle propriétaire pour exécuter son code, ce qui rendait la traduction plus complexe car il fallait comprendre le fonctionnement interne de cette machine virtuelle afin de parvenir à modifier les différents scripts compilés.

Le contexte de ce projet était donc la nécessité de comprendre en profondeur le fonctionnement de la machine virtuelle utilisée par le jeu, ainsi que les mécanismes de traduction de texte, pour pouvoir créer un patch de traduction fonctionnel.

## Étapes de réalisation

La réalisation de mon patch de traduction pour le jeu a impliqué plusieurs étapes clés, notamment :

1. **Analyse de la machine virtuelle du jeu** : J'ai utilisé mes compétences en programmation système, telles que l'assembleur et le code machine, pour analyser le fonctionnement interne de la machine virtuelle utilisée par le jeu. Cela a nécessité une compréhension approfondie de la structure et du fonctionnement de la machine virtuelle afin de pouvoir identifier les zones de code à modifier pour ajouter la fonctionnalité de traduction.
2. **Documentation de la communauté** : Je me suis appuyé sur la documentation et les ressources disponibles dans la communauté du jeu pour comprendre le fonctionnement de la machine virtuelle propriétaire utilisée. Cela a inclus la recherche en ligne de tutoriels, de forums et de discussions techniques pour obtenir des informations supplémentaires sur la manière de créer un patch de traduction pour ce type de jeu.
3. **Utilisation de l'API de DeepL pour la traduction** : J'ai utilisé l'API de DeepL, un service de traduction en ligne, pour traduire les textes du jeu dans la langue souhaitée. Cela a nécessité l'obtention d'une clé d'API auprès de DeepL et l'intégration de cette API dans le code Java de mon patch pour automatiser le processus de traduction des textes du jeu. Une fois traduit, je n'avais plus besoin de l'API et je pouvais juste distribuer le résultat.
4. **Création du patch en Java** : J'ai utilisé mes compétences en Java pour créer le patch de traduction en modifiant le code de la machine virtuelle du jeu. J'ai utilisé Java afin de modifier le code du jeu pour remplacer les textes d'origine par le texte souhaité.
5. **Test et débogage** : Une fois le patch de traduction créé, j'ai procédé à des tests rigoureux pour m'assurer que les traductions étaient correctes et que le patch fonctionnait comme prévu. Cela a impliqué de jouer au jeu avec le patch appliqué pour vérifier que les textes étaient bien traduits et que le jeu fonctionnait sans bug ni erreur. Cependant, il s'avère qu'il y a encore des problèmes d'affichage lorsque le texte traduit est bien plus long que le texte original.
6. **Améliorations éventuelles** : Les éléments de l'interface ne sont actuellement pas traduits. Ces textes sont gérés différemment et certains sont même de simples images. Ces modifications requièrent de plus amples modifications du code et il est bien plus compliqué de tester le bon fonctionnement à cause de la longueur du jeu en question.

Une fois toutes les étapes de réalisation complétées et les tests réussis, mon patch de traduction a été considéré comme fonctionnel et prêt à être distribué pour permettre à mes amis de profiter du jeu en anglais.

## Acteurs

Dans ce projet, j'était seul et je me suis appuyé sur la documentation réalisée par la communauté pour comprendre le fonctionnement de la machine virtuelle et réaliser la traduction. Les ressources en ligne, les forums de discussion et les tutoriels ont été des sources importantes d'information pour comprendre les défis techniques et les meilleures pratiques de la création d'un patch de traduction pour ce jeu.

## Résultats

Le patch de traduction a été créé avec succès et a fonctionné correctement pour traduire les textes du jeu dans la langue cible. Les textes traduits ont été générés à l'aide de l'outil de traduction en ligne DeepL, qui a permis d'obtenir des traductions plus fiables pour les textes du jeu. La traduction ne vaut clairement pas celle qui pourrait être réalisée par un humain, mais elle permet aux personnes ayant joué aux précédents opus de comprendre le scénario.

## L'avenir du projet dans un futur immédiat et à distance

Dans un futur immédiat, le patch de traduction peut être amélioré en continu pour corriger d'éventuels bugs ou pour ajouter de nouvelles fonctionnalités, comme la traduction des interfaces ou l'amélioration de la qualité des traductions.

## Mon Regard Critique

En tant qu'auteur de ce patch de traduction, j'ai pu apporter une valeur ajoutée en utilisant mes compétences en programmation système, notamment en assembleur et en code machine, pour comprendre le fonctionnement interne de la machine virtuelle utilisée par le jeu. J'ai également utilisé mes compétences en Java pour créer le patch de traduction et intégrer l'API de DeepL pour la traduction des textes.

Ce projet m'a permis d'acquérir de nouvelles compétences en ingénierie inverse. J'ai également appris à utiliser des API de traduction en ligne pour obtenir des traductions de qualité pour les textes du jeu.

En conclusion, la réalisation de ce patch de traduction a été un projet enrichissant qui m'a permis d'appliquer mes compétences en programmation système, en Java et en ingénierie inverse pour créer un patch fonctionnel. Je suis fier d'avoir pu contribuer à rendre le jeu lisible à un public anglophone en le traduisant.

### Compétences

 - [Ingénierie inverse](https://nagispace.gitlab.io/skill/reverse-engineering/)
 - [Système](https://nagispace.gitlab.io/skill/prog-system/)
 - [Java](https://nagispace.gitlab.io/skill/java/)
 - [Autonomie](https://nagispace.gitlab.io/skill/autonomous/)
