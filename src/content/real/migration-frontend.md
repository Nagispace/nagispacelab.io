---
title: Migration d'un frontend vers Angular
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Angular
 - Autonomie
 - Relation utilisateur
 - UX
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094134651469176832/angular.png
description: Migrer un frontend afin de ne pas être exposé aux failles de vulnérabilité 
layout: '../../layouts/BlogPost.astro'
type: 'real'
---

# Migration d'un Frontend AngularJS vers Angular 12

## Présentation du projet

Le projet de migration d'un frontend AngularJS vers Angular 12 était une tâche complexe mais passionnante que j'ai réalisée en collaboration avec mon équipe de développement. En tant qu'expert et avec une solide expérience dans le développement frontend, j'ai été chargé de mener cette migration pour moderniser un projet existant basé sur AngularJS et le mettre à jour vers la dernière version stable d'Angular, la version 12.

## Objectif et contexte

L'objectif de cette migration était de bénéficier des avantages de la dernière version d'Angular, qui offre des performances améliorées, une meilleure sécurité et de nouvelles fonctionnalités pour le développement frontend. Le contexte du projet était également important, car le frontend existant basé sur AngularJS n'allait plus recevoir de mises à jour de sécurité. Ces mises à jour sont nécessaires pour garantir la sécurité de l'utilisateur lors de sa navigation. Ces mises à jour peuvent notamment corriger des failles permettant des attaques de type Cross-Site Scripting (XSS) ou des attaques de contrefaçon de requêtes inter-sites (CSRF), ces attaques peuvent être utilisées pour voler des informations d'identification, rediriger les utilisateurs vers des sites malveillants, voler des cookies de session ou encore tromper les utilisateurs pour qu'ils effectuent des actions non voulues sur une application. La migration vers Angular 12 était donc nécessaire pour garantir la pérennité et la stabilité du projet à long terme.

## Étapes de réalisation

La migration d'un frontend AngularJS vers Angular 12 a été réalisée en plusieurs étapes clés :

1. **Évaluation de l'existant** : J'ai commencé par analyser en détail le code et l'architecture du frontend AngularJS existant pour comprendre son fonctionnement, identifier les dépendances et les parties obsolètes et évaluer la complexité de la migration.
2. **Planification et préparation** : Ensuite, j'ai élaboré un plan détaillé de la migration, en identifiant les tâches à réaliser, les ressources nécessaires et les délais. J'ai également préparé l'environnement de développement en installant les outils et les dépendances nécessaires pour travailler avec Angular 12.
3. **Mise à jour des dépendances** : La première étape de la migration a été de mettre à jour les dépendances du projet, notamment les librairies tierces et les modules internes utilisés par l'application.
4. **Refactorisation du code** : J'ai ensuite procédé à la refactorisation du code AngularJS existant pour le rendre compatible avec Angular 12. Cela a inclus la mise à jour de la syntaxe, la modification des APIs obsolètes et la réorganisation du code pour se conformer aux meilleures pratiques d'Angular 12.
5. **Tests et validation** : Une fois la migration terminée, j'ai effectué des tests approfondis pour vérifier le bon fonctionnement de l'application dans différentes situations et configurations. J'ai également sollicité les retours de l'équipe pour m'assurer que l'application répondait bien aux besoins et aux attentes.
6. **Collaboration avec l'équipe** : Bien que j'aie réalisé une preuve de concept seul, la migration a été un travail d'équipe. J'ai collaboré étroitement avec les autres membres de l'équipe de développement pour résoudre les problèmes rencontrés, échanger sur les meilleures pratiques et valider les choix architecturaux.
7. **Mise en production** : Une fois que l'application migrée a été validée, nous l'avons déployée en production et avons commencé à l'utiliser pour les besoins réels du client.

## Acteurs

La migration d'un frontend AngularJS vers Angular 12 a été réalisée en collaboration avec différents acteurs au sein de l'équipe de développement. En tant qu'expert, j'ai joué un rôle clé dans la conception et la mise en œuvre de la migration. J'ai travaillé en étroite collaboration avec les autres développeurs (Carlos et Inmaculada) et les responsables de projet (Maxime, le business analyste, Thierry l'architecte et Christophe, le lead tech), pour garantir le succès de la migration. Ma connaissance approfondie d'Angular et mon expertise technique ont été essentielles pour résoudre les défis techniques et architecturaux rencontrés tout au long du processus de migration.

Les retours de l'équipe ont également été importants pour le succès de la migration. Les commentaires et les suggestions de l'équipe de développement ont été pris en compte pour améliorer l'expérience utilisateur (UX) et garantir l'accessibilité du site web.

## Résultats

La migration du frontend AngularJS vers Angular 12 a été un succès. Le nouveau frontend fonctionne bien et nous l'utilisons encore à ce jour dans notre application. Les nouvelles fonctionnalités offertes par Angular 12 ont également permis d'améliorer la qualité du code, la maintenabilité de l'application et la sécurité des données.

En outre, la collaboration étroite avec l'équipe de développement a permis d'optimiser les délais de migration et de minimiser les impacts sur les utilisateurs finaux.

## Avenir du projet

Dans un futur immédiat, le projet de migration du frontend AngularJS vers Angular 12 continuera à bénéficier de l'utilisation de la dernière version d'Angular et des avantages qu'elle offre en termes de performances, de sécurité et de fonctionnalités. L'application migrée continuera d'être utilisée par l'entreprise pour répondre aux besoins du client et sera régulièrement mise à jour pour rester à jour avec les nouvelles versions d'Angular.

Aujourd'hui, l'application migrée est utilisée de manière quotidienne par les utilisateurs finaux. L'équipe de développement continue de surveiller l'application pour s'assurer de sa stabilité et de sa fiabilité et pour résoudre rapidement tout problème qui pourrait survenir tout en rajoutant de nouvelles fonctionnalités selon les demandes du client.

## Mon regard critique

En tant que responsable de la migration du frontend AngularJS vers Angular 12, j'ai apporté une réelle valeur ajoutée au projet. Mon expertise en Angular et ma connaissance approfondie des meilleures pratiques de développement frontend ont été essentielles pour mener à bien cette migration complexe. J'ai travaillé en étroite collaboration avec l'équipe de développement pour résoudre les défis techniques et architecturaux et pour garantir que l'application migrée répondait aux besoins et aux attentes des utilisateurs finaux. De plus, j'ai su rapidement remonter les problèmes qui sont survenus lors de la migration.

Ce projet de migration m'a permis d'acquérir de nouvelles compétences en matière de migration de technologies frontend et de gestion de projets de grande envergure. J'ai appris à anticiper les défis potentiels liés à la migration et à proposer des solutions adaptées pour les surmonter. J'ai également renforcé ma compréhension de l'importance de la collaboration avec les différents acteurs d'un projet, notamment les développeurs, les responsables, les architectes et les clients, pour garantir la réussite d'une migration.

En revue, ce projet de migration a été une expérience enrichissante qui m'a permis d'acquérir de nouvelles compétences, de renforcer ma collaboration avec l'équipe de développement et d'améliorer continuellement l'expérience utilisateur de l'application migrée.

## Enseignements tirés

La migration d'un frontend AngularJS vers Angular 12 a été un processus complexe qui a nécessité une planification minutieuse, une expertise technique approfondie et une collaboration étroite avec l'équipe de développement. Quelques enseignements importants peuvent être tirés de cette expérience :

1. La planification est clé : La migration d'une technologie frontend nécessite une planification minutieuse pour identifier les défis potentiels, anticiper les impacts sur les utilisateurs finaux et établir un plan d'action approprié.
2. La collaboration est essentielle : Travailler en étroite collaboration avec son équipe et les clients est crucial pour garantir la réussite d'une migration. Les retours et les suggestions de ces acteurs peuvent contribuer à améliorer la qualité de l'application migrée.
3. La connaissance technique est primordiale : Une expertise technique approfondie dans les technologies frontend, telles qu'Angular, est essentielle pour résoudre les défis techniques et architecturaux rencontrés lors d'une migration.
4. L'expérience utilisateur est centrale : L'UX de l'application migrée est un élément clé du succès du projet. Prendre en compte les besoins et les attentes des utilisateurs finaux et apporter des améliorations continues à l'interface utilisateur, à la navigation et à la performance, est crucial.
5. L'adaptabilité est nécessaire : La migration d'une technologie frontend peut être complexe et imprévisible. Il est important d'être adaptable et de pouvoir prendre des décisions rapides pour surmonter les défis inattendus qui peuvent survenir en cours de route.
6. L'apprentissage continu est important : La technologie frontend évolue rapidement, il est donc essentiel de continuer à apprendre et à se former pour rester à jour avec les dernières versions et les meilleures pratiques de développement.
7. La communication est clé : Communiquer régulièrement et efficacement avec l'équipe de développement, les testeurs et le client est essentiel pour assurer la réussite d'une migration. La transparence, la clarté et la disponibilité sont des éléments importants pour s'assurer que toutes les parties prenantes sont informées du progrès, des défis et des décisions prises tout au long du processus de migration.

### Compétences

 - [Angular](https://nagispace.gitlab.io/skill/angular/)
 - [Autonomie](https://nagispace.gitlab.io/skill/autonomous/)
 - [Relation utilisateur](https://nagispace.gitlab.io/skill/relation-ux/)
 - [Anglais](https://nagispace.gitlab.io/skill/my-english-is-as-rich-as-my-tailor/)
