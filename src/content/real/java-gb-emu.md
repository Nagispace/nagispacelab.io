---
title: Émulateur GameBoy Java
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Java
 - Système
 - Technique
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1095231718736285777/java_gb_emu.gif
description: Les avantages et la portabilité de Java pour étudier le GameBoy
layout: '../../layouts/BlogPost.astro'
type: 'real'
---

# Réalisation d'un émulateur GameBoy en Java

### Présentation du projet

Dans le cadre de mes études en informatique, j'ai décidé de réaliser un projet personnel consistant à développer un émulateur GameBoy en Java. J'ai choisi ce projet car je suis passionné par la programmation et les jeux vidéo et que j'étais à la recherche d'un défi.

### Objectif et contexte

L'objectif de ce projet était de développer un émulateur de GameBoy qui permettrait de jouer à des jeux sur un ordinateur. Le contexte était de réaliser ce projet seul, en utilisant des ressources disponibles en ligne et en appliquant mes compétences en programmation système (comprendre l'assembleur et le code machine) et Java.

### Étapes et réalisation

La réalisation de cet émulateur a été divisée en plusieurs étapes, chacune impliquant la compréhension de différents concepts clés de l'architecture GameBoy et de l'émulation. Tout d'abord, j'ai dû étudier la documentation de la CPU de la GameBoy et comprendre les opcodes de l'assembleur spécifique à cette plateforme. Ensuite, j'ai implémenté la CPU et la mémoire de la GameBoy en Java.

Pour implémenter les instructions CPU, j'ai créé un objet de type "Instructions" qui possède plusieurs constructeurs en fonction des besoins des différentes instructions du CPU. Lorsqu'une instruction est exécutée, l'objet vérifie d'abord le mode d'adressage de l'instruction, puis effectue les opérations nécessaires en utilisant les registres du CPU. Chaque instruction est placée à l'indice correspondant à son Opcode dans un tableau. Ainsi, lorsqu'une instruction est appelée, l'émulateur vérifie l'adresse de l'Opcode pour accéder directement à l'instruction associée. Cette méthode permet une exécution rapide et efficace des instructions. Cette méthode m'a permis de séparer clairement les instructions de la logique principale de l'émulateur, rendant ainsi le code plus facile à maintenir et à déboguer.

Pour effectuer l'opération en Java, j'ai utilisé des opérations logiques et arithmétiques comme les opérateurs "&", "|", "^", "<<", ">>", "+", "-", "*", "/", etc. Pour accéder aux valeurs stockées dans les registres, j'ai utilisé un tableau de 8 bits pour représenter les 8 registres différents du GameBoy.

J'ai également implémenté les instructions qui modifient les drapeaux du CPU. Ces drapeaux sont des indicateurs qui reflètent l'état du CPU après une opération. Par exemple, le drapeau "Z" indique si le résultat de l'opération est zéro, le drapeau "C" indique s'il y a une retenue lors d'une opération arithmétique, etc.

Enfin, j'ai testé chaque instruction individuellement pour m'assurer qu'elle fonctionne correctement. Pour cela, j'ai utilisé des ROMs de test qui vérifient si les résultats obtenus par l'émulateur sont les mêmes que ceux obtenus par un vrai GameBoy.

Enfin, j'ai testé mon émulateur en utilisant des ROMs de jeux GameBoy. Bien que l'interface utilisateur, l'affichage du jeu, l'entrée utilisateur et la sortie audio ne soient pas encore implémentés, j'ai pu vérifier que les opcodes étaient correctement exécutés et que les jeux se lançaient sans problème.

Toutes les implémentations ont été réalisées en Java, en utilisant les connaissances acquises lors de mes précédents projets de programmation système et en m'appuyant sur la documentation réalisée par la communauté.

En travaillant sur ce projet, j'ai pu renforcer mes compétences en programmation système, en particulier en comprenant l'assembleur spécifique au GameBoy, ainsi qu'en Java. J'ai également appris à travailler de manière autonome et à résoudre des problèmes techniques de manière efficace.

Malgré le fait que l'émulateur ne soit pas encore totalement fonctionnel, j'ai réussi à atteindre mon objectif initial qui était de comprendre l'architecture de la console GameBoy et l'émulation. À l'avenir, j'ai l'intention de poursuivre le développement de cet émulateur en implémentant l'interface utilisateur, l'affichage du jeu, l'entrée utilisateur et la sortie audio.

### Acteurs

Je travaillais seul sur ce projet, mais j'ai pu me baser sur des ressources en ligne, telles que des forums de discussion et des documentations réalisées par la communauté.

### Résultats

Bien que l'émulateur ne soit pas encore terminé, ce qui est implémenté marche bien et je suis satisfait des résultats. J'ai appris beaucoup de choses en travaillant sur ce projet, notamment en ce qui concerne la programmation système et l'émulation de matériel.

### Avenir du projet

Dans un futur immédiat, je prévois de poursuivre le développement de cet émulateur pour ajouter de nouvelles fonctionnalités et améliorer la compatibilité avec les jeux. À plus long terme, j'aimerais ajouter des fonctionnalités telles que l'affichage LCD, le son, les entrées utilisateurs, le support du multijoueur et des outils de débogage des ROMs.

### Mon regard critique

Ce projet m'a permis de développer mes compétences en programmation système, en particulier en ce qui concerne la compréhension de l'assembleur et du code machine. J'ai également amélioré mes compétences en Java, en particulier en ce qui concerne l'utilisation de structures de données telles que la réflexion de Class, les tableaux et les listes chaînées, ainsi que la manipulation de bits et d'octets. Cependant, il y a encore beaucoup de travail à faire pour finaliser l'émulateur et le rendre pleinement fonctionnel.

Dans l'avenir immédiat, je prévois de continuer à travailler sur ce projet pour améliorer la précision de l'émulateur et ajouter des fonctionnalités telles que l'affichage du jeu et l'entrée utilisateur. À plus long terme, je pourrais également envisager d'ajouter la prise en charge d'autres plates-formes de jeu dans l'émulateur.

Dans l'ensemble, je suis très satisfait de ce projet et des compétences que j'ai acquises en le réalisant. Il m'a permis de développer ma curiosité et mon intérêt pour la programmation système et m'a donné une expérience précieuse dans le développement de projets complexes en Java.

### Compétences

*-* [Java](https://nagispace.gitlab.io/skill/java/)
*-* [Programmation système](https://nagispace.gitlab.io/skill/prog-system/)
