---
title: Maitrise de l'Anglais
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Transverse
 - Anglais
 - Communication
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094134650315751425/mr.worldwide.png
description: Pouvoir communiquer partout dans le monde
layout: '../../layouts/BlogPost.astro'
type: 'skill'
level: ⭐⭐⭐⭐⭐
---

# Maitrise de l'anglais
⭐⭐⭐⭐⭐
## Ma définition

La compétence en langue anglaise est une aptitude qui permet de comprendre, de communiquer, d'écrire et de lire en anglais dans un contexte professionnel. Cette compétence est primordiale pour les professionnels qui travaillent dans un environnement international, pour les personnes qui voyagent régulièrement ou pour les individus qui souhaitent évoluer dans leur carrière en ayant accès à des informations ou des ressources en anglais.

## Mes éléments de preuve

En tant qu'ingénieur logiciel, ma compétence en anglais a été mise en pratique dans plusieurs situations professionnelles. L'une d'entre elles a été la migration d'un Backend applicatif Java 8 vers Java 17 pour l'un de mes projets.

Dans ce contexte, j'étais responsable de la migration et je devais communiquer avec mes collègues internationaux une fois terminé. J'ai ainsi réalisé une présentation en anglais pour expliquer les changements et les bénéfices de la migration, ainsi qu'une documentation détaillée en anglais pour guider mes collègues dans le processus de migration.

Mes collègues ont pu effectuer la migration de manière fluide grâce à la documentation que j'ai produite. Ma valeur ajoutée dans cette situation a été de combiner mes compétences en ingénierie logicielle et en anglais pour mener à bien cette migration, en assurant une communication claire et une compréhension mutuelle entre les différents membres de l'équipe.

Cet exemple concret prouve que ma compétence en anglais est essentielle pour travailler efficacement dans un contexte international et pour communiquer avec des collègues non-francophones. Cela m'a également permis de prendre des responsabilités importantes dans des projets impliquant des partenaires internationaux, renforçant ainsi ma position en tant qu'ingénieur logiciel compétent et fiable. [En savoir plus sur cette migration.](https://nagispace.gitlab.io/real/migration-java/)

## Mon autocritique

En ce qui concerne ma compétence en anglais, je considère que je parle couramment la langue et que je suis capable de communiquer avec des interlocuteurs internationaux sans difficulté majeure. Toutefois, je suis conscient que mon accent peut parfois être un obstacle pour une compréhension optimale. 

Je considère que le seul moyen de m'améliorer actuellement est de travailler mon accent en parlant à des natifs et en étudiant des aspects avancés tels que la littérature et l'histoire de la langue anglaise. En effet, la langue anglaise possède des subtilités et des nuances qui diffèrent grandement du français, tant sur le plan grammatical que sémantique. La littérature anglaise est également très riche et variée, ce qui me permettrait d'approfondir ma compréhension de la langue et d'enrichir mon vocabulaire.

Je suis également conscient que la langue anglaise est omniprésente dans le domaine de l'ingénierie logicielle, que ce soit pour la documentation, la communication avec des partenaires étrangers ou la participation à des conférences internationales. Par conséquent, je considère que la maîtrise de l'anglais est une compétence cruciale pour tout ingénieur logiciel et je m'efforce de maintenir un niveau élevé dans ce domaine.

À partir de mon expérience, je conseillerais à quiconque cherchant à améliorer sa compétence en anglais de commencer par écouter des médias en anglais autant que possible. Cela peut être des podcasts, des vidéos, des émissions de télévision ou de radio, etc. Le but est de s'habituer à entendre la langue parlée de manière naturelle et de comprendre l'intonation et la prononciation.

Ensuite, je recommande de pratiquer régulièrement la lecture en anglais, en commençant par des textes simples et en augmentant progressivement la difficulté. Il est également important de prendre note des mots et expressions nouvelles pour les intégrer à son vocabulaire.

Enfin, je pense qu'il est utile de s'exposer à la culture anglaise en général. Cela permet d'acquérir une compréhension plus profonde de la langue et de sa façon d'être utilisée dans différentes situations.

## Mon évolution dans cette compétence

En ce qui concerne ma compétence en anglais, je suis actuellement à un niveau avancé et je parle couramment la langue. Toutefois, je considère que mon objectif à long terme est d'atteindre le niveau natif. Pour y parvenir, je continue à me concentrer sur l'amélioration de mon accent, ainsi que sur l'étude des subtilités de la littérature anglaise et des différences culturelles avec la langue française. Bien que je n'aie pas prévu de suivre de cours ou de séminaires en anglais dans un futur proche ou lointain, je suis convaincu que je peux améliorer ma compétence en m'immergeant davantage dans la langue à travers des livres, des films, des émissions de radio et des conversations avec des anglophones natifs. 

## Réalisations

- ### [Migration technique d'un backend](https://nagispace.gitlab.io/real/migration-java/) 

- ### [Migration d'un frontend vers Angular](https://nagispace.gitlab.io/real/migration-frontend/)
