---
title: Méthode agile
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Transverse
 - Agile
 - Communication
 - Travail en équipe
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094135206706946108/agile.png
description: Une méthodologie de projet moderne et efficace
level: ⭐⭐⭐⭐
layout: '../../layouts/BlogPost.astro'
type: 'skill'
---

# Méthodologie Agile (en SCRUM)
⭐⭐⭐⭐
## Ma définition

La méthodologie "Agile" est une approche de gestion de projet qui vise à favoriser la collaboration, l'adaptabilité et la réactivité face aux changements. Elle est particulièrement adaptée pour les projets qui nécessitent une forte interaction avec les clients ou les utilisateurs finaux, ainsi que pour les projets où les exigences et les spécifications évoluent régulièrement. Le cadre le plus couramment utilisé en méthodologie "Agile" est le Scrum, qui est composé de cérémonies, d'artefacts et de rôles clés.

En tant qu'expert en ingénierie, ma compétence en méthodologie "Agile" et plus particulièrement en Scrum, consiste à maîtriser les différentes cérémonies et artefacts qui y sont associés, à comprendre les rôles clés et les responsabilités associées à chacun d'entre eux et à être capable de travailler efficacement en équipe pour atteindre les objectifs fixés lors de chaque sprint. Cela implique également d'être capable de faciliter la communication et la collaboration entre les différents membres de l'équipe et de maintenir une attitude positive et motivante tout au long du projet.

## Mes éléments de preuve

Je peux citer une anecdote où j'ai utilisé la cérémonie de la rétrospective de Sprint pour proposer des améliorations dans la façon dont on communique pour éviter les pertes d'informations lorsque l'on s'absente. En effet, lors de cette rétrospective, nous avons souligné le problème de communication en cas d'absence prévue ou non. Parmi les solutions proposées, la mienne a été retenue et appliquée avec succès à ce jour.

En outre, j'ai également participé activement à d'autres cérémonies telles que la planification de Sprint, les rétrospectives de Sprint et les réunions quotidiennes de SCRUM, en aidant l'équipe à déterminer les objectifs de chaque Sprint et à suivre leur progression, à identifier les obstacles et à les résoudre en équipe.

## Mon autocritique

Je reconnais qu'il y a encore une marge de progression pour moi en ce qui concerne la méthodologie "Agile", en particulier en ce qui concerne SCRUM. Bien que j'aie une bonne compréhension des principes et des pratiques de base, je sais qu'il y a encore beaucoup à apprendre et à expérimenter pour parfaire mes compétences. Je suis conscient que l'utilisation de la méthodologie "Agile" peut varier selon les projets et les équipes, donc il est important d'être en mesure d'adapter les pratiques aux besoins spécifiques de chaque projet.

En tant qu'expert en ingénierie logiciel, je considère que la méthodologie "Agile" est une compétence importante à maîtriser, car elle permet de garantir un processus de développement de logiciel efficace et collaboratif. Cela est particulièrement important dans le contexte de projets complexes et de grande envergure, où une gestion efficace de l'équipe et des ressources est essentielle pour garantir des résultats de qualité. Je considère donc la méthodologie "Agile" comme une compétence de base pour tout ingénieur travaillant dans le développement de logiciel.

## Mon évolution dans cette compétence

Bien que je ne souhaite pas devenir Scrum Master, je considère que la méthodologie "Agile" est une compétence importante pour un ingénieur logiciel. Je continuerai donc à participer activement aux différentes cérémonies "Agile" et à mettre en pratique mes connaissances en SCRUM. Cependant, je ne prévois pas de suivre de formation spécifique aux méthodes "Agile" à court terme. Je suis convaincu que l'apprentissage continu et l'autoformation sont essentiels pour progresser dans cette compétence.

## Réalisations

- [Migration d'un frontend vers Angular](https://nagispace.gitlab.io/real/migration-frontend/)
- [Migration technique d'un backend en Java](https://nagispace.gitlab.io/real/migration-java/)
