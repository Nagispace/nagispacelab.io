---
title: Angular
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Angular
 - Technique
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094134651469176832/angular.png
description: Un Frontend moderne
layout: '../../layouts/BlogPost.astro'
type: 'skill'
level: ⭐⭐⭐⭐
---

# Angular
⭐⭐⭐⭐
### Définition 

Angular est un framework open-source de développement web, créé et maintenu par Google. Il a été initialement développé en 2010 sous le nom d'AngularJS, mais a été entièrement réécrit en 2016 sous le nom d'Angular. Depuis lors, il est devenu l'un des frameworks les plus populaires pour le développement web front-end.

Angular est basé sur le langage TypeScript, qui est une extension de JavaScript. Il est utilisé pour développer des applications web complexes, dynamiques et interactives, en offrant une architecture modulaire et une gestion de l'état centralisé. Il facilite également la création d'applications mobiles à l'aide de la plate-forme Ionic, qui utilise Angular comme base de développement.

### Contexte professionnel

Angular est largement utilisé dans le développement d'applications web de grande envergure, telles que les applications de gestion de contenu, les plateformes de commerce électronique et les applications bancaires en ligne. Il est également populaire auprès des développeurs qui travaillent sur des projets open-source, en raison de son code source ouvert et de sa grande communauté de développeurs.

Dans le contexte professionnel, la connaissance d'Angular est de plus en plus demandée. Les entreprises recherchent des développeurs Angular qualifiés pour travailler sur des projets de développement web front-end. En tant que tel, il est devenu un élément essentiel du portefeuille de compétences pour les développeurs web modernes. Les développeurs qui maîtrisent Angular ont également la possibilité de travailler sur des projets à forte valeur ajoutée, ce qui peut se traduire par des opportunités d'emplois bien rémunérés.

### Anecdote

Le développement d'applications web internationales est un défi important pour les développeurs. Il est essentiel de prendre en compte les différences culturelles et linguistiques pour offrir une expérience utilisateur optimale. Lorsque j'ai travaillé sur une application frontend Angular pour une entreprise internationale, j'ai été confronté à un défi intéressant : créer une directive Angular personnalisée pour permettre l'internationalisation de l'application.

La première étape était de comprendre comment les applications Angular gèrent l'internationalisation. J'ai découvert que le framework fournit un module appelé "@angular/localize" qui permet aux développeurs de traduire le contenu de l'application en différentes langues. Cependant, j'ai réalisé que le processus de traduction était fastidieux et qu'il serait difficile de maintenir manuellement toutes les traductions pour chaque langue.

Pour résoudre ce problème, j'ai décidé de créer une directive personnalisée qui permettait de simplifier le processus de traduction. La directive, appelée "translate", permettait de marquer le texte qui doit être traduit en utilisant un attribut. Le texte marqué serait alors traduit automatiquement dans la langue de l'utilisateur.

Cependant, la création de la directive personnalisée s'est avérée plus difficile que prévu. J'ai dû travailler avec des contraintes techniques importantes, notamment la nécessité d'utiliser un attribut personnalisé et de maintenir l'accessibilité de l'application pour les utilisateurs ayant des lecteurs d'écran.

Après de nombreux essais et erreurs, j'ai finalement réussi à créer une directive personnalisée qui répondait à tous les besoins de l'application. La directive était facile à utiliser et permettait de traduire automatiquement tous les textes marqués en utilisant les fichiers de traduction appropriés.

Le résultat final était une solution simple et élégante pour permettre l'internationalisation de l'application. Les développeurs n'avaient plus besoin de gérer manuellement les traductions et les utilisateurs pouvaient facilement passer d'une langue à l'autre.

Cette expérience m'a appris que les défis de développement ne sont pas toujours techniques. La compréhension des besoins des utilisateurs et la réflexion créative sont souvent nécessaires pour créer d'élégantes solutions efficaces. Avec l'utilisation de directives personnalisées, j'ai pu simplifier considérablement le processus d'internationalisation de l'application et offrir une expérience utilisateur exceptionnelle pour les utilisateurs. [En savoir plus sur cette migration ici.](https://nagispace.gitlab.io/real/migration-frontend/)

### Progression et contexte

En tant que développeur Angular avancé, je suis fier de pouvoir créer des applications web robustes et réactives. Cependant, je ne considère pas avoir atteint un niveau de maîtrise complet sur Angular. Le framework est en constante évolution et il y a toujours de nouvelles fonctionnalités et des techniques à apprendre. 

Dans le contexte professionnel, Angular est un choix populaire pour les grandes entreprises qui ont besoin d'applications web puissantes et réactives. Il est particulièrement adapté aux applications qui nécessitent une interface utilisateur complexe et une interactivité élevée, telles que les applications d'entreprise, les outils de gestion de projet et les systèmes de gestion de contenu.

En outre, Angular est également une bonne option pour les applications mobiles hybrides qui sont souvent créées en utilisant des technologies web. Le framework fournit des fonctionnalités pour créer des applications mobiles réactives et performantes qui fonctionnent sur plusieurs plates-formes.

Je ne considère pas la création du design de l'application comme étant spécifique à Angular. De ce fait, quel que soit le contexte (mobile ou ordinateur de bureau), Angular fonctionne de la même manière.

### Mon profil et responsabilités

Dans ma quête de disposer d'un large éventail d'outils à ma disposition, je considère qu'il est essentiel de maîtriser Angular en tant que framework. J'ai une solide connaissance de la syntaxe TypeScript et je suis capable de concevoir des architectures logicielles efficaces pour les applications web complexes. Je maîtrise également les concepts clés d'Angular tels que les modules, les services, les composants, les directives et les pipes. 

### Recul et conseils

L'un des aspects les plus importants de la pratique d'Angular est de rester à jour avec les nouvelles fonctionnalités et les bonnes pratiques. Le framework évolue constamment et il est essentiel de suivre les mises à jour pour rester à la pointe de la technologie. De plus, en apprenant les bonnes pratiques et les modèles de conception recommandés, les développeurs peuvent améliorer la qualité et la maintenabilité de leur code. 

Un conseil important est de ne pas avoir peur de poser des questions et de demander de l'aide lorsque vous rencontrez des problèmes. Le développement d'applications web avec Angular peut être complexe et il est important de ne pas se sentir isolé dans ses projets. Les développeurs devraient chercher à travailler en équipe et à communiquer efficacement avec leurs collègues pour résoudre les problèmes et améliorer leur pratique.

### Avenir

### Projet professionnel, niveau souhaité

Tout d'abord, il est important de comprendre que le choix d'un framework dépend des besoins spécifiques du projet. Angular est un excellent choix pour les projets nécessitant une application web dynamique et interactive, mais d'autres frameworks comme React et Vue peuvent également être adaptés en fonction des exigences du projet. Je ne compte donc pas me spécialiser dans Angular et je considère que mon niveau actuel est amplement suffisant.

### Formations/autoformations

Même si je ne compte pas me spécialiser dans Angular, il est important pour moi, ainsi que pour les projets sur lesquels je travaille, de me maintenir à jour sur les nouveautés de ce framework. Il n'y a pas de formation en particulier actuellement, mais je compte bien suivre les évolutions.

## Réalisations

- [Migration d'un frontend vers Angular](https://nagispace.gitlab.io/real/migration-frontend/)
