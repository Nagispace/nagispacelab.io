---
title: Java
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Java
 - Technique
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094134651959918672/java.png
description: "Write once, run anywhere"
layout: '../../layouts/BlogPost.astro'
type: 'skill'
level: ⭐⭐⭐⭐⭐
---

# Java

⭐⭐⭐⭐⭐

### Définition 

Java, est un langage de programmation orienté objet inspiré du C++. Il a été créé par James Gosling et Patrick Naughton, employés de Sun Microsystems en 1995. Il permet de réaliser des programmes pouvant s'exécuter sur n'importe quelle machine et architecture sans devoir y effectuer un quelconque changement. 

### Contexte professionnel

Il existe de nombreux contextes professionnels dans lequel Java peu intervenir.

1. Développement de logiciels d'entreprise : Java est souvent utilisé pour développer des logiciels d'entreprise en raison de sa capacité à gérer de grandes quantités de données et à intégrer facilement avec d'autres technologies. Des applications telles que les systèmes de gestion de la relation client (CRM), les systèmes de gestion de contenu (CMS), les systèmes de gestion des ressources humaines (HRMS) et les systèmes de gestion des stocks (IMS) sont souvent développés en Java.
2. Développement de jeux vidéo : Java peut être utilisé pour développer des jeux vidéo pour ordinateurs et appareils mobiles. Java offre de bonnes performances et une bonne gestion de la mémoire, ce qui en fait un choix populaire pour les développeurs de jeux.
3. Développement de logiciels pour appareils mobiles : Java est largement utilisé pour développer des applications mobiles Android. La plate-forme Android utilise le langage Java comme l'un de ses principaux langages de programmation.
4. Développement d'applications web : Java est souvent utilisé pour développer des applications web côté serveur en raison de sa capacité à gérer des tâches lourdes telles que la gestion des sessions, l'authentification des utilisateurs et la gestion des transactions.
5. Développement de logiciels scientifiques et de calcul : Java est également utilisé dans des domaines tels que la science, la recherche et l'analyse de données, car il offre une grande précision et une grande flexibilité pour la manipulation de données complexes.

Outre ces cas d'utilisation, selon les statistiques de GitHub de 2020, java est le troisième langage le plus populaire derrière le JavaScript et le python. Il est donc naturel qu'il soit une compétence recherchée et appréciée sur le marché du travail.

### Anecdote(s)

Le projet le plus imposant mettant en œuvre cette compétence est Mon Identifiant SNCF (MID).
Il s'agit de la brique d'authentification et gestion des comptes clients de la SNCF. Sur ce projet, j'ai réalisé avec succès la migration technique de Java de la version 8 vers la version 17. La demande venait du client et de l'équipe sécurité afin d'avoir une meilleur sécurité et maintenabilité de notre application. Bien que compatible à un niveau de syntaxe de base, j'ai rapidement découvert que le code de l'application avait été écrit en utilisant des bibliothèques tierces qui étaient maintenant obsolètes et incompatibles avec la nouvelle version de Java. Cela signifiait que je devais soit trouver des alternatives à ces bibliothèques, soit les mettre à jour pour qu'elles soient compatibles avec Java 17. Cela s'est avéré être un défi de taille, car une bibliothèque interne qui générait nos class d'interface Java pour l'API n'était pas compatible avec Java 17 et aucune mise à jour de cette dernière n'était prévue. De ce fait, il a fallu que je modifie la façon dont l'API fonctionnait dans le code. L'une des solutions aurait été de refaire cette librairie nous même, mais la solution n'a pas été retenue car la dette technique aurait été bien trop lourde. La solution que j'ai mise en place consiste à utiliser une librairie déjà existante et de la configurer selon nos besoins. Cela nous a permis de nous détacher de cette librairie interne mais il fallait désormais que je change le fonctionnement de l'entièreté de notre API et comment cette dernière répondait aux différentes requêtes. [En savoir plus sur cette migration ici.](https://nagispace.gitlab.io/real/migration-java/)

### Compétence

#### Niveau

Je considère mon niveau en Java proche d'expert. Je suis parfaitement autonome dessus, j'ai pu réaliser de nombreux projets divers avec. Cependant je pense que je peux atteindre un niveau plus élevé en multipliant mes expériences personnelles.

#### Progression et contexte

Je connais déjà de nombreuses facettes de la programmation en Java ; cependant il m'en reste encore une multitude à découvrir pour pouvoir réellement me considérer comme un expert. Je m'intéresse particulièrement au Bytecode java et aux Mixins. La connaissance du Bytecode permet en théorie de "modifier" une application déjà compilée, et elle est essentielle pour utiliser de manière avancée les Mixins. Les Mixins en Java permettent de modifier dynamiquement le comportement d'un objet en lui ajoutant ou en lui retirant des comportements à l'exécution, sans avoir besoin de recompiler le code source.
Cela peut être utile dans certaines situations, par exemple pour adapter un objet à un contexte spécifique ou pour ajouter une fonctionnalité supplémentaire temporairement sans modifier l'implémentation de la classe.

De part sa versatilité, il est impossible d'explorer toutes les facettes de Java avec un seul projet. La façon dont j'utilise Java dépend entièrement du besoin.

#### Mon profil et responsabilités

En tant qu'ingénieur logiciel, j'ai la responsabilité de comprendre et analyser les besoins, de conceptualiser une architecture logiciel, de développer et de documenter. Il est donc de mon devoir de comprendre les différents outils disponibles afin de faire un choix réfléchi lors du développement d'une application. L'importance de Java en tant que langage de programmation réside dans le fait qu'il est l'un des langages de programmation les plus populaires et largement utilisés pour le développement de logiciels. Avoir des compétences en Java est donc très important pour les ingénieurs logiciels, car cela leur permet d'être compétitifs sur le marché du travail et de répondre aux besoins des entreprises qui cherchent des ingénieurs logiciels qualifiés. 

### Vitesse d'acquisition 

J'ai commencé à apprendre le Java en 2013 sur la version 7 afin de réaliser des mods Minecraft. Après cette année, mes opportunités d'utiliser Java se sont limitées, ce qui a ralenti ma progression jusqu'en 2016. Une fois les bases du langage bien acquises cette fois-ci je suis rapidement monté en compétence, le langage m'intriguait de plus en plus. Que ce soit pour réaliser des serveurs web, applications Android ou encore des applications pour ordinateurs de bureau simples. 

### Recul et conseils

Nous sommes désormais en 2023 et Minecraft a été racheté par Microsoft depuis bien longtemps et la différence technique entre chaque nouvelle version est désormais plus stable que lorsque j'avais commencé. J'utilisais Eclipse, "un chewing-gum et deux bouts de ficelle" pour réaliser et apprendre Java sur l'objectif de créer un mod pour la version 1.4.2 du jeu. Les ressources étaient faibles et très souvent pas à jour de par les nombreux et fréquents changements du code de Minecraft et des librairies nécessaires. L'environnement dans lequel j'ai commencé avec Eclipse et Java était extrêmement peu accueillant pour les débutants, ce qui a initialement suscité en moi une certaine aversion envers ces outils.

Bien que les conditions se soient améliorées, je ne conseille pas ce chemin pour commencer car le bagage technique est très lourd à appréhender et les changements techniques sont souvent sources de frustration. Je recommande plutôt la création d'une application console ou d'un autre projet ne nécessitant pas d'interface graphique complexe pour apprendre la base du langage ; suivi d'une Web App afin d'apprendre les Framework les plus importants tout en travaillant d'autres compétences techniques. Je recommande également de ne pas utiliser Eclipse. Bien que suffisant et gratuit, ce dernier devient assez vite limitant et peu performant pour les projets les plus imposants. Les étudiants peuvent bénéficier d'une licence gratuite pour IntelliJ, qui est de loin l'IDE le plus complexe, performant et complet pour le développement de tout ce qui touche de près ou de loin à du Java. Il existe également une version communautaire **gratuite** mais avec moins de fonctionnalités que sa version payante. 

### Avenir

### Projet professionnel, niveau souhaité 

J'ai un attrait particulier pour les défis qui exigent une résolution de problèmes complexes. Je souhaite que Java soit l'un des outils à ma disposition. Bien que j'apprécie fortement ce dernier et que je désire devenir un expert sur Java, il me semble bien plus important de diversifier mes compétences dans des langages plus modernes tel que Kotlin, qui s'exécute aussi sur la JVM ou encore Rust qui permet d'avoir des Web server et API bien plus performants, surtout dans le contexte d'application devant gérer des millions d'utilisateurs chaque jour.

### Formations/autoformations

Hors des libraires, je prévois d'approfondir mes compétences sur le Bytecode java et les Mixins à l'exécution. Il y a également tout l'aspect cyber-sécurité qui lui dépend de l'implémentation de la JVM en elle même. OpenJDK et Amazon Corretto sont deux exemples et implémentent différents aspects sur la cyber-sécurité.

## Réalisations

- [Émulateur GameBoy](https://nagispace.gitlab.io/real/java-gb-emu/)

- [Migration d'un Backend](https://nagispace.gitlab.io/real/migration-java/)

- [Création d'un patch de traduction](https://nagispace.gitlab.io/real/mtl/)
