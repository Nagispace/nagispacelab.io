---
title: Ingénierie inverse
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Technique
 - Ingénierie inverse
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094134650600955984/reverse-engineering.png
description: Analyser le résultat pour retracer le chemin.
layout: '../../layouts/BlogPost.astro'
type: 'skill'
level: ⭐⭐
---

## Ma compétence en ingénierie inverse
⭐⭐

L'ingénierie inverse est une compétence qui consiste à analyser des systèmes, des programmes ou des dispositifs complexes afin de comprendre leur fonctionnement interne. Dans un contexte professionnel, elle est souvent utilisée pour améliorer des produits ou des processus existants, ou pour trouver des vulnérabilités et des failles de sécurité dans des systèmes.

Personnellement, je me suis intéressé à l'ingénierie inverse pour des projets personnels. J'ai notamment eu l'occasion d'utiliser cette compétence pour modifier des jeux vidéo, en modifiant leurs comportements ou en modifiant certaines informations.

### Mes éléments de preuve

Dans l'un de mes projets personnels, j'ai eu l'opportunité de travailler sur un jeu vidéo qui utilisait une machine virtuelle propriétaire. Grâce à mes compétences en ingénierie inverse, j'ai pu comprendre le fonctionnement des Opcodes des différents scripts et modifier les dialogues sans impacter le reste du jeu. Le résultat final était un jeu entièrement traduit, offrant une expérience plus agréable pour mes amis qui ne parlaient pas la langue d'origine.

Dans le cadre de mon projet de création d'un émulateur GameBoy, j'ai utilisé l'ingénierie inverse pour comprendre comment la console fonctionne et comment les instructions sont exécutées. En examinant le code machine des ROM de jeux GameBoy, j'ai pu déterminer comment les différents processeurs de la console communiquent entre eux et comment ils gèrent la mémoire. Cette compréhension approfondie de la façon dont le GameBoy fonctionne m'a permis de créer un émulateur plus précis et plus fiable.

### Mon autocritique

Bien que je sois capable d'utiliser l'ingénierie inverse pour des projets personnels, je reste débutant dans ce domaine. Il y a encore beaucoup à apprendre et à explorer, en particulier dans des contextes professionnels. De plus, je pense que cette compétence peut varier considérablement selon les situations et les projets, il est donc important de rester curieux et d'acquérir de l'expérience dans divers domaines.

Cependant, j'aime me donner des défis pour progresser et j'ai hâte de continuer à développer mes compétences en ingénierie inverse. Pour ceux qui souhaitent acquérir cette compétence, je recommande de se concentrer sur des projets personnels pour commencer, de pratiquer régulièrement et de rester curieux.

### Mon évolution dans cette compétence

Actuellement, je ne suis pas inscrit à des cours en ligne pour approfondir ma connaissance de l'ingénierie inverse, cependant j'ai découvert une nouvelle manière de m'exercer et de progresser dans cette compétence grâce aux challenges "Catch The Flag". Ces compétitions sont organisées en ligne et consistent en la résolution de problèmes de sécurité informatique qui nécessitent souvent l'utilisation de l'ingénierie inverse pour trouver la solution. Participer à ces CTFs me permet de me confronter à des problèmes complexes dans un environnement ludique et stimulant, ce qui m'aide à améliorer mes compétences d'ingénierie inverse de manière autonome. J'espère ainsi continuer à progresser dans cette compétence tout en découvrant de nouveaux horizons en matière de sécurité informatique.

Je prévois également de continuer à travailler sur des projets personnels pour pratiquer mes compétences en ingénierie inverse et pour découvrir de nouveaux domaines. Enfin, j'espère pouvoir collaborer avec d'autres professionnels de l'ingénierie inverse pour apprendre de leur expérience et améliorer mes compétences.

## Réalisations

- ### [Création d'un patch de traduction](https://nagispace.gitlab.io/real/mtl/)
