---
title: Programmation système
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Système
 - Technique
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094134651167195226/system-prog.png
description: Tout ce qui englobe la programmation de bas niveau
layout: '../../layouts/BlogPost.astro'
type: 'skill'
level: ⭐⭐
---

# Programmation système : compétence clé de l'ingénierie logiciel
⭐⭐

La programmation système est une compétence clé de l'ingénierie logiciel qui permet de concevoir, de développer et de maintenir des logiciels système (virtuels ou non). Cette compétence consiste à maîtriser les technologies et les langages de programmation utilisés pour créer des systèmes d'exploitation, des pilotes de périphériques, des machines virtuelles, des émulateurs et des compilateurs.

## Mes éléments de preuve

En matière de programmation système, j'ai développé une compétence particulière pour la création d'émulateurs et de machines virtuelles. Un exemple concret de ma maîtrise de cette compétence est la création d'un émulateur GameBoy en Java.

Grâce à mes connaissances en programmation système, j'ai pu comprendre en profondeur le fonctionnement de la console GameBoy, notamment son architecture matérielle et son fonctionnement interne. J'ai ainsi pu développer un émulateur capable de simuler le comportement de la console de manière très fidèle.

Pour cela, j'ai utilisé des techniques avancées de programmation système, telles que la manipulation de registres matériels et la mise en place d'une émulation du processeur Z80. J'ai également étudié en détail les spécifications techniques de la console, ce qui m'a permis de concevoir un émulateur capable de gérer efficacement les échanges entre la RAM et la ROM et la gestion des "banques" selon le modèle de GameBoy.

La création de cet émulateur m'a permis d'appliquer mes compétences en programmation système de manière concrète et de résoudre des problèmes complexes liés à l'émulation d'une console de jeux. Cela a également renforcé ma compréhension des concepts de programmation système et m'a permis d'acquérir de nouvelles connaissances dans ce domaine.

## Mon autocritique

Je considère que je maîtrise la programmation système à un niveau moyen. Bien que j'aie déjà réalisé des projets concrets dans ce domaine, je pense qu'il me reste encore beaucoup à apprendre et à explorer. En particulier, je souhaiterais approfondir mes connaissances en assembleur et en optimisation de code, ce qui me permettrait d'optimiser davantage mes émulateurs ou encore de mieux comprendre le fonctionnement de certaines machines virtuelles propriétaires.

La programmation système est une compétence essentielle dans mon profil d'expert en ingénierie logiciel. Elle me permet de concevoir des outils performants et efficaces pour résoudre des problèmes complexes liés à l'informatique. Je suis donc motivé à continuer à développer cette compétence et à l'appliquer dans mes projets futurs.

## Mon évolution dans cette compétence

À moyen terme, je souhaite approfondir ma connaissance de la programmation système en me concentrant sur des projets plus complexes, tels que le développement d'un compilateur. Je suis actuellement en train de suivre des formations en ligne pour améliorer mes compétences en programmation système, notamment sur l'architecture des processeurs, la gestion de la mémoire, les pilotes de périphériques et les systèmes de fichiers. Je suis également en train de travailler sur un projet de machine virtuelle pour approfondir ma maîtrise de cette compétence.

## Réalisations

- ### [Création d'un patch de traduction](https://nagispace.gitlab.io/real/mtl/)

- ### [Émulateur GameBoy](https://nagispace.gitlab.io/real/java-gb-emu/)
