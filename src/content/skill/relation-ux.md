---
title: Relation utilisateur
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Transverse
 - Relation utilisateur
 - Communication
 - UX
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094134651720847411/feedback.png
description: Comprendre et communiquer avec les utilisateurs
layout: '../../layouts/BlogPost.astro'
type: 'skill'
level: ⭐⭐⭐
---

# Relation utilisateur
⭐⭐⭐

La compétence en relation utilisateur est essentielle pour tout professionnel travaillant dans le domaine de l'ingénierie logicielle. Elle permet de comprendre les besoins des utilisateurs et de créer des produits et des solutions qui répondent à leurs attentes.

## Ma définition

La compétence en relation utilisateur consiste à être capable de comprendre les besoins et les attentes des utilisateurs d'un produit ou d'une solution et de concevoir des solutions qui répondent à ces besoins. Elle implique également d'être en mesure de communiquer efficacement avec les utilisateurs pour comprendre leurs retours et leurs suggestions.

## Mes éléments de preuve

Lors de la création de mon robot Discord, j'ai intégré un système de retours utilisateurs. Les utilisateurs peuvent donner leur avis sur le robot en utilisant une commande spéciale. J'ai utilisé ces retours pour améliorer l'expérience utilisateur en apportant des modifications au robot. Par exemple, j'ai ajouté de nouvelles fonctionnalités en réponse aux demandes des utilisateurs et j'ai également corrigé des erreurs signalées par les utilisateurs. Grâce à cette compétence, j'ai pu améliorer l'expérience utilisateur et rendre le robot plus convivial.

## Mon autocritique

Je considère que je suis à un niveau moyen dans ma maîtrise de la compétence en relation utilisateur. Bien que j'aie réussi à améliorer l'expérience utilisateur de mon robot Discord, je pense que je pourrais encore progresser dans ma compréhension des besoins des utilisateurs et dans ma capacité à concevoir des solutions qui répondent à ces besoins. La contextualisation de cette compétence peut également varier en fonction des projets, des produits ou des solutions. C'est pourquoi je pense que je devrais continuer à travailler sur cette compétence pour pouvoir l'adapter à toutes les situations.

Dans mon profil d'expert en ingénierie, la compétence en relation utilisateur est très importante car elle est essentielle pour garantir la satisfaction des clients et la réussite des projets. Elle doit donc être prioritaire dans mon travail. J'aime me donner des défis pour progresser et je suis convaincu que je peux encore améliorer mes compétences en relation utilisateur. Pour cela, je vais continuer à travailler sur des projets où je pourrai mettre en pratique cette compétence et suivre des formations pour approfondir mes connaissances dans ce domaine.

## Mon évolution dans cette compétence

Dans le cadre de mon projet professionnel, je souhaite devenir un expert en ingénierie logicielle. Pour atteindre cet objectif, je pense qu'il est important de continuer à travailler sur ma compétence en relation utilisateur. Je prévois de suivre des formations en design thinking et en expérience utilisateur pour approfondir mes connaissances et pour pouvoir concevoir des solutions qui répondent aux besoins des utilisateurs de manière efficace et efficiente. Je prévois également de travailler sur des projets où je pourrai mettre en pratique cette compétence, en collaboration avec des équipes multidisciplinaires.

## Réalisations

- ### [Migration d'un frontend vers Angular](https://nagispace.gitlab.io/real/migration-frontend/)

- ### [Création d'un robot Discord](https://nagispace.gitlab.io/real/discord-bot/)

