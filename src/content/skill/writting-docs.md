---
title: Création de documentations
pubDate: 03/03/2023
author: "Nicolas Coulon"
tags:
 - Transverse
 - Transfert de connaissances
 - Communication
imgUrl: https://media.discordapp.net/attachments/720610453608333324/1094053837460996176/writting-docs.png
description: Savoir partager son savoir à tous
layout: '../../layouts/BlogPost.astro'
type: 'skill'
level: ⭐⭐⭐⭐
---

# Création de documentations
⭐⭐⭐⭐

### Définition

La compétence d'écriture de documentation technique est la capacité à communiquer efficacement des informations techniques claires et précises à un public varié. Les documents techniques peuvent inclure des manuels d'utilisation, des guides de référence, des spécifications techniques, des instructions d'installation, des notes de mise à jour et d'autres types de documents destinés à aider les utilisateurs à comprendre et à utiliser un produit ou un service.

### Contexte professionnel

La maîtrise de la rédaction de documentation technique est essentielle pour assurer le succès de tout projet de développement de logiciels. Les rédacteurs de documentation technique sont responsables de la création de documents tels que des manuels d'utilisation, des guides de référence, des spécifications techniques, des instructions d'installation et des notes de mise à jour, qui permettent aux utilisateurs de comprendre et d'utiliser efficacement un produit.

### Anecdote

Lors de mon travail sur un projet en entreprise, j'ai été amené à recevoir un transfert de connaissances de notre configuration et implémentation d'OpenLDAP par notre architecte logiciel. Nous n'avions pas de documentation sur ce sujet précis et les informations étaient importantes. Afin de pouvoir me familiariser avec OpenLDAP j'ai donc passé du temps avec notre architecte tout en écrivant ladite documentation. Par la suite ma documentation a été revue par notre architecte afin que je puisse l'améliorer et la préciser d'avantage. 

### Progression et contexte

La compétence d'écriture de documentation technique est de plus en plus importante dans le monde du développement de logiciels. Cela est dû à l'importance croissante de la communication et de la collaboration au sein des équipes de développement de logiciels, ainsi qu'à la nécessité d'assurer la qualité et la maintenabilité du code. 

Je peux encore m'améliorer davantage en travaillant ma capacité à synthétiser les informations importantes de façon claire. Je considère que j'ai un niveau moyen dans ce domaine. L'application de cette compétence dépend principalement de mon niveau de compétence dans la technologie pour laquelle j'écris la documentation.

### Mon profil et responsabilités

L'un des rôles clés d'un ingénieur logiciel est de créer des solutions informatiques pour répondre aux besoins des clients. Cependant, la responsabilité de l'ingénieur logiciel ne se limite pas à la simple création de logiciels ; il est également responsable de la documentation technique. Que ce soit pour valider une documentation ou pour en écrire une.

### Recul et conseils

En tant qu'ingénieur logiciel, j'ai rapidement compris l'importance de la documentation technique pour la réussite des projets de développement de logiciels. Cependant, j'ai réalisé que la compétence d'écriture de documentation technique est souvent sous-estimée ou considérée comme secondaire par rapport à la création de logiciels.

Au fil des ans, j'ai travaillé dur pour améliorer mes compétences en matière d'écriture de documentation technique. J'ai appris que la documentation technique est une compétence complexe qui nécessite de la pratique et une bonne compréhension de la communication technique. Il est important d'utiliser un langage simple et facile à comprendre, tout en étant précis et technique pour éviter toute confusion. J'ai appris qu'il est également essentiel de fournir des exemples concrets pour faciliter la compréhension.

Une autre difficulté que j'ai rencontrée est de trouver le temps pour écrire de la documentation technique. Étant donné que les ingénieurs logiciels ont souvent des délais serrés pour terminer les projets, écrire de la documentation technique peut être considéré comme une tâche secondaire. Cependant, j'ai compris que cela pouvait causer des problèmes plus tard et ralentir le processus de développement si la documentation technique n'était pas prête à temps.

### Avenir

### Projet professionnel, niveau souhaité

Que ce soit pour mon projet professionnel ou personnel, il est important pour moi de travailler ma capacité à communiquer de façon concise des informations techniques avancées. C'est l'une des compétences qui, selon moi, permet de différencier un bon développeur d'un excellent développeur. 

### Formations/autoformations

J'ai la chance d'avoir réalisé mon alternance dans une entreprise multinationale qui nous fournit de nombreuses ressources à ce sujet. J'ai déjà participé à des workshops en équipe pour travailler ces différents aspects. Je compte bien participer à plus de ces sessions ainsi qu'à plus de formations à ce sujet.

## Réalisations

- ### [Migration technique d'un backend en Java](https://nagispace.gitlab.io/real/migration-java/)
